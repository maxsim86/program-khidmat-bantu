from django.db import models
from profiles.models import Profile
from bantuan.models import JenisBantuan

# Create your models here.


class ProductionLine(models.Model):
    name = models.CharField(max_length=120)
    team_leader = models.ForeignKey(Profile, on_delete=models.CASCADE)
    bantuan = models.ManyToManyField(JenisBantuan)

    def __str__(self):
        return self.name
