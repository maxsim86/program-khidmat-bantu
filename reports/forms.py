from django import forms
from .models import Report, ProblemReported
from django.shortcuts import get_object_or_404
from areas.models import ProductionLine




class ReportResultForm(forms.Form):
    dapatkan_laporan = forms.ModelChoiceField(
        queryset=ProductionLine.objects.all())
    hari = forms.CharField(widget=forms.DateTimeInput(
        attrs={'class': 'datepicker'}
    ))


class ReportSelectLineForm(forms.Form):
    dapatkan_laporan = forms.ModelChoiceField(
        queryset=ProductionLine.objects.none(), label='')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        # print(user)
        super(ReportSelectLineForm, self).__init__(*args, **kwargs)
        self.fields['dapatkan_laporan'].queryset = ProductionLine.objects.filter(
            team_leader__user__username=user)


class ReportForm(forms.ModelForm):

    class Meta:
        model = Report
        # fields = '__all__'
        exclude = ('user', 'dapatkan_laporan',)

    def __init__(self, *args, **kwargs):
        dapatkan_laporan = kwargs.pop('dapatkan_laporan', None)
        super().__init__(*args, **kwargs)
        if dapatkan_laporan is not None:
            line = get_object_or_404(ProductionLine, name=dapatkan_laporan)
            # print(line.name)
            self.fields['skop'].queryset = line.products.all()


class ProblemReportedForm(forms.ModelForm):

    class Meta:
        model = ProblemReported
        # fields = '__all__'
        exclude = ('user', 'report', 'problem_id')
