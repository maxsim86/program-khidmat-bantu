import random
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from bantuan.models import JenisBantuan
from categories.models import Kategori
from areas.models import ProductionLine
from django.urls import reverse
from django.db.models import Sum, Avg
import random
from datetime import datetime




# Create your models here.

# hours= (
#     ("1", "1"),
#     ("2", "2"),
#     ("3", "3"),
#     ...
# )
#dapatkan report
hours = ([(str(x), str(x)) for x in range(1, 9)])

el = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
      'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

'''
get_queryset() - method responsible for retrieving all the data from the database according to
               the defined querysets
super()        -  method which returns a proxy object that allows you to refer parent class

def get_all_active(self):
    return super().get_queryset().filter(draft=False)
    return Post.objects.filter(draft=False)
'''


class ReportQueryset(models.QuerySet):
    def get_by_line_and_hari(self, hari, line_id):
        return self.filter(hari=hari, dapatkan_laporan__id=line_id)


class ReportManager(models.Manager):
    def get_queryset(self):
        return ReportQueryset(self.model, using=self._db)

    def get_by_line_and_hari(self, hari, line_id):
        return self.get_queryset().get_by_line_and_hari(hari, line_id)

class Report(models.Model):
    nama_sekolah = models.CharField(max_length=45, blank=True)
    kod_sekolah = models.CharField(max_length=7, blank= True)
    hari = models.DateField(default=timezone.now)
    hingga = models.DateField(default=timezone.now)
    waktu_mula = models.CharField(max_length=2, choices=hours)
    waktu_tamat = models.CharField(max_length=2, choices=hours)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    skop = models.ForeignKey(JenisBantuan, on_delete=models.CASCADE)
    dapatkan_laporan = models.ForeignKey(ProductionLine, on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    objects = ReportManager()

    def get_hari(self):
        return self.hari.strftime('%Y/%m/%d')

    def get_absolute_url(self):
        return reverse("reports:update-view", kwargs={'dapatkan_laporan': self.dapatkan_laporan,'pk': self.pk, })

    def __str__(self):
        return "{}-{}-{}".format(self.waktu_mula, self.waktu_tamat, self.dapatkan_laporan, self.waktu_tamat, self.nama_sekolah, self.kod_sekolah)

    class Meta:
        ordering = ('-created',)


def random_code():
    random.shuffle(el)
    code = [str(x) for x in el[:12]]
    str_code = ''.join(code)

    return str_code


class ProblemReportedManager(models.Manager):
    def get_problems_by_hari_and_line(self, hari, line):
        return super().get_queryset().filter(report__hari=hari, report__dapatkan_laporan__name=line)

    def problems_from_today(self):
        now = datetime.now().strftime('%Y-%m-%d')
        return super().get_queryset().filter(report__hari=now)


class ProblemReported(models.Model):
    kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE)
    isu  = models.TextField()
    problem_id = models.CharField(max_length=12, unique=True, blank=True, default=random_code)
    breakdown = models.PositiveIntegerField()
    umum = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    
    objects = ProblemReportedManager()

    def __str__(self):
        return "{}-{}".format(self.kategori.name, self.isu[:20])

    class Meta:
        verbose_name = "Report Masalah"
        verbose_name_plural = "Report Masalah"
