from django import forms
from .models import GeneralPost, Comment
from django.core.validators import ValidationError


class PostForm(forms.ModelForm):
    penerangan = forms.CharField(widget=forms.Textarea(attrs={'rows': 3}))

    class Meta:
        model = GeneralPost
        fields = ('tajuk', 'penerangan', 'imej')

    def clean_penerangan(self):
        desc = self.cleaned_data.get('penerangan')
        if len(desc) < 10:
            raise ValidationError("Penerangan terlalu pendek")
        return desc

class CommentForm(forms.ModelForm):
    body = forms.CharField(label='', widget=forms.Textarea(attrs={'rows': 4, 'placeholder': 'komen anda disini...'}))

    class Meta:
        model = Comment
        fields = ('body',)
